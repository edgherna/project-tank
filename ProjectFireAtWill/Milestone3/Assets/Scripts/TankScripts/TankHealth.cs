﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankHealth : MonoBehaviour
{
    public float maxHealth = 10;
    public float currentHealth = 10;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    void OnTriggerEnter(Collider other)
    {
        //The amount of health for the tank.
        if (other.gameObject.CompareTag("Bullet"))
        {
            currentHealth--;
            Destroy(other.gameObject);

            if (currentHealth <= 0)
            {
                Destroy(other.gameObject);
                Destroy(this.gameObject);
            }
        }

        //This for the health pick up.
        if (other.gameObject.CompareTag("Health"))
        {
            currentHealth++;
            Destroy(other.gameObject);
        }
    }
}
