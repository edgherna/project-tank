﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : MonoBehaviour
{
    public TankData pawn;
    public List<Transform> waypoints;
    public int currentWaypoint;
    public float waypointCutoff;
    public bool isPatrolling = true;

    //More Patrolling Details
    public enum WaypointOrder { Loop, PingPong, Random, Stop };
    public WaypointOrder waypointOrder;
    public bool isAdvancingWaypoints = true;

    public bool showNextWayPoint = false;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (isPatrolling)
        {
            Patrol();
        }
    }

    void Patrol()
    {
        Vector3 targetPosition = new Vector3(waypoints[currentWaypoint].position.x,
                                             pawn.mover.tf.position.y,
                                             waypoints[currentWaypoint].position.z);

        Vector3 currentPosition = pawn.mover.tf.position;
        Vector3 dirToWaypoint = targetPosition - currentPosition;
        pawn.mover.TurnTowards(dirToWaypoint);
        pawn.mover.Move(pawn.mover.tf.forward);

        if (Vector3.Distance(pawn.mover.tf.position, targetPosition) <= waypointCutoff)
        {
            DetermineNextWayPointAdvanced();
        }
    }

    void DetermineNextWayPointAdvanced()
    {
        //Advance to the next Waypoint.
        if (waypointOrder == WaypointOrder.PingPong)
        {
            if (isAdvancingWaypoints)
            {
                currentWaypoint++;
            }
            else
            {
                currentWaypoint--;
            }
        }
        else if (waypointOrder == WaypointOrder.Random)
        {
            int prevWaypoint = currentWaypoint;
            while (prevWaypoint == currentWaypoint)
            {
                currentWaypoint = Random.Range(0, waypoints.Count);
            }
        }
        else
        {
            currentWaypoint++;
        }

        //Deals with being at the last waypoint.
        if (currentWaypoint >= waypoints.Count)
        {
            if (waypointOrder == WaypointOrder.Loop)
            {
                // Loop
                currentWaypoint = 0;
            }
            else if (waypointOrder == WaypointOrder.PingPong)
            {
                // Reverse directions
                isAdvancingWaypoints = false;
                currentWaypoint = currentWaypoint - 2;
            }
            else if (waypointOrder == WaypointOrder.Stop)
            {
                isPatrolling = false;
            }
        }
        else if (currentWaypoint < 0)
        {
            // Reverse directions (to go forward again)
            isAdvancingWaypoints = true;
            currentWaypoint = 1;
        }
    }
}