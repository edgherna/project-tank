﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletData : MonoBehaviour
{
    public float moveSpeed = 1.0f;
    public float damageDone = 0;
    public float lifespan = 3.0f;
    public TankData shooter = null;


    private void Start()
    {
        Destroy(gameObject, lifespan);
    }
}
