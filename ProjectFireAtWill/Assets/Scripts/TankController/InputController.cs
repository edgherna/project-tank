﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    public TankData pawn;

    KeyCode forwardKey;
    KeyCode backwardKey;
    KeyCode turnRightKey;
    KeyCode turnLeftKey;
    KeyCode shootKey;

    public enum InputScheme { WASD, arrowKeys };
    public InputScheme input = InputScheme.WASD;

    // Use this for initialization
    public void Start()
    {
        GameManager.instance.player = this;
    }

    public void SetInputKeys()
    {
        switch (input)
        {
            case InputScheme.arrowKeys:
                forwardKey = KeyCode.UpArrow;
                backwardKey = KeyCode.DownArrow;
                turnRightKey = KeyCode.RightArrow;
                turnLeftKey = KeyCode.LeftArrow;
                shootKey = KeyCode.Return;
                break;

            case InputScheme.WASD:
                forwardKey = KeyCode.W;
                backwardKey = KeyCode.S;
                turnRightKey = KeyCode.D;
                turnLeftKey = KeyCode.A;
                shootKey = KeyCode.Space;
                break;
        }
    }

    // Update is called once per frame
    public void Update()
    {
        SetInputKeys();

        if (Input.GetKey(forwardKey))
        {
            pawn.mover.Move(pawn.mover.tf.forward);
        }
        if (Input.GetKey(backwardKey))
        {
            pawn.mover.Move(-pawn.mover.tf.forward);
        }
        if (Input.GetKey(turnRightKey))
        {
            pawn.mover.Turn(1);
        }
        if (Input.GetKey(turnLeftKey))
        {
            pawn.mover.Turn(-1);
        }
        //This is to limit the shooting sequence of the tank.
        if (Input.GetKeyDown(shootKey))
        {
            pawn.shooter.Shoot();
        }
    }
}
