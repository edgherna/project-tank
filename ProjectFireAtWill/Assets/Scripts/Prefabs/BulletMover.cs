﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMover : MonoBehaviour
{

    private Transform tf;
    private BulletData data;

    // Use this for initialization
    void Start()
    {
        tf = GetComponent<Transform>();
        data = GetComponent<BulletData>();
    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }

    void Move()
    {
        tf.position += tf.forward * data.moveSpeed * Time.deltaTime;
    }
}
