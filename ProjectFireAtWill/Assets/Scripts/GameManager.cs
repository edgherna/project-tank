﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    public static GameManager instance;

    //public GameObject StartCanvas, OnePlayerCanvas, TwoPlayerCanvas, 
    //                  WinCanvas, WinPlayerOneCanvas, WinPlayerTwoCanvas, LoseCanvas;

    public InputController player;

    //public enum GameState
    //{
    //    Start, //0
    //    OnePlayer, //1
    //    TwoPlayer, //2
    //    Win, //3
    //    Win1, //4
    //    Win2, //5
    //    Lose //6
    //}

    //public GameState gameState;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(instance);
        }
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    //void ActivateScreens()
    //{
    //    switch(gameState)
    //    {
    //        case GameState.Start:
    //            StartCanvas.SetActive(true);
    //            OnePlayerCanvas.SetActive(false);
    //            TwoPlayerCanvas.SetActive(false);
    //            WinCanvas.SetActive(false);
    //            WinPlayerOneCanvas.SetActive(false);
    //            WinPlayerTwoCanvas.SetActive(false);
    //            LoseCanvas.SetActive(false);
    //            break;

    //        case GameState.OnePlayer:
    //            StartCanvas.SetActive(false);
    //            OnePlayerCanvas.SetActive(true);
    //            TwoPlayerCanvas.SetActive(false);
    //            WinCanvas.SetActive(false);
    //            WinPlayerOneCanvas.SetActive(false);
    //            WinPlayerTwoCanvas.SetActive(false);
    //            LoseCanvas.SetActive(false);
    //            break;

    //        case GameState.TwoPlayer:
    //            StartCanvas.SetActive(false);
    //            OnePlayerCanvas.SetActive(false);
    //            TwoPlayerCanvas.SetActive(true);
    //            WinCanvas.SetActive(false);
    //            WinPlayerOneCanvas.SetActive(false);
    //            WinPlayerTwoCanvas.SetActive(false);
    //            LoseCanvas.SetActive(false);
    //            break;

    //        case GameState.Win:
    //            StartCanvas.SetActive(false);
    //            OnePlayerCanvas.SetActive(false);
    //            TwoPlayerCanvas.SetActive(false);
    //            WinCanvas.SetActive(true);
    //            WinPlayerOneCanvas.SetActive(false);
    //            WinPlayerTwoCanvas.SetActive(false);
    //            LoseCanvas.SetActive(false);
    //            break;

    //        case GameState.Win1:
    //            StartCanvas.SetActive(false);
    //            OnePlayerCanvas.SetActive(false);
    //            TwoPlayerCanvas.SetActive(false);
    //            WinCanvas.SetActive(false);
    //            WinPlayerOneCanvas.SetActive(true);
    //            WinPlayerTwoCanvas.SetActive(false);
    //            LoseCanvas.SetActive(false);
    //            break;

    //        case GameState.Win2:
    //            StartCanvas.SetActive(false);
    //            OnePlayerCanvas.SetActive(false);
    //            TwoPlayerCanvas.SetActive(false);
    //            WinCanvas.SetActive(false);
    //            WinPlayerOneCanvas.SetActive(false);
    //            WinPlayerTwoCanvas.SetActive(true);
    //            LoseCanvas.SetActive(false);
    //            break;

    //        case GameState.Lose:
    //            StartCanvas.SetActive(false);
    //            OnePlayerCanvas.SetActive(false);
    //            TwoPlayerCanvas.SetActive(false);
    //            WinCanvas.SetActive(false);
    //            WinPlayerOneCanvas.SetActive(false);
    //            WinPlayerTwoCanvas.SetActive(false);
    //            LoseCanvas.SetActive(true);
    //            break;
    //    }
    //}

    //public void SetGameState(int state)
    //{
    //    gameState = (GameState)state;
    //    ActivateScreens();
    //}
}
