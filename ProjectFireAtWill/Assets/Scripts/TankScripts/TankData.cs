﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankData : MonoBehaviour
{
    public float moveSpeed;
    public float originSpeed;
    public float turnSpeed;
    public float shootRate;
    public float originShootRate;
    public float damageDone;
    public float bulletSpeed;
    public GameObject bulletPrefab;
    public float cooldownShootRate;
    public float cooldownMoveSpeed;

    //Other Tank Components
    [HideInInspector] public TankMover mover;
    [HideInInspector] public TankShooter shooter;
    [HideInInspector] public TankHealth health;

    // Use this for initialization
    void Start ()
    {
        mover = GetComponent<TankMover>();
        shooter = GetComponent<TankShooter>();
        health = GetComponent<TankHealth>();
        cooldownShootRate = 0;
        originSpeed = moveSpeed;
        originShootRate = shootRate;
	}

    void Update()
    {
        //This is for the cooldown of fireRate or speedup.
        if (moveSpeed != originSpeed)
        {
            cooldownMoveSpeed--;
        }

        if (cooldownMoveSpeed <= 0)
        {
            cooldownMoveSpeed = 0;
        }

        if (shootRate != originShootRate)
        {
            cooldownShootRate--;
        }

        if (cooldownShootRate <= 0)
        {
            cooldownShootRate = 0;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("SpeedUp"))
        {
            Destroy(other.gameObject);
            moveSpeed++;
            cooldownMoveSpeed = 20;
        }

        if (other.gameObject.CompareTag("FireRate"))
        {
            Destroy(other.gameObject);
            shootRate++;
            cooldownShootRate = 20;
        }
    }
}
