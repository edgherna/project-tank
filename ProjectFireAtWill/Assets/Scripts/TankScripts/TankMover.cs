﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TankMover : MonoBehaviour
{
    //This is the information needed to execute the tank moving process.
    private CharacterController cc;
    [HideInInspector] public Transform tf;
    private TankData data;

    // Use this for initialization
    void Start()
    {
        //This makes it so that we gather the information needed to control the tank.
        cc = GetComponent<CharacterController>();
        tf = GetComponent<Transform>();
        data = GetComponent<TankData>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Move(Vector3 moveVector)
    {
        //This makes it so that the tank moves.
        cc.SimpleMove(moveVector.normalized * data.moveSpeed);
    }

    public void Turn(float direction)
    {
        //Turn in the direction at turn speed
        tf.Rotate(0, Mathf.Sign(direction) * data.turnSpeed * Time.deltaTime, 0);
    }

    public void TurnTowards(Vector3 targetDirection)
    {
        //This is to make the tank turn.
        Quaternion targetRotation = Quaternion.LookRotation(targetDirection);

        //This is so that the rotation happens.
        tf.rotation = Quaternion.RotateTowards(tf.rotation, targetRotation, data.turnSpeed * Time.deltaTime);
    }
}
