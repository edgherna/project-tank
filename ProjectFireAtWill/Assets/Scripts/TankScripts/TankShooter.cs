﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankShooter : MonoBehaviour
{
    private TankData data;
    public float nextFireTime;
    public Transform firePoint;

    // Use this for initialization
    void Start()
    {
        data = GetComponent<TankData>();

        nextFireTime = Time.time;
    }

    public void Shoot()
    {
        if (Time.time > nextFireTime)
        {
            //Fire the bullet
            GameObject bullet = Instantiate(data.bulletPrefab, firePoint.position, firePoint.rotation) as GameObject;

            //Set the appropriate data
            BulletData bulletData = bullet.GetComponent<BulletData>();
            bulletData.damageDone = data.damageDone;
            bulletData.moveSpeed = data.bulletSpeed;

            //Set the next fire time
            nextFireTime = Time.time + data.shootRate;
        }
    }
}
