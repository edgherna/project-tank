﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelConstructor : MonoBehaviour
{
    //This is to give out the measurements to the map.
    public int rowsSize;
    public int colsSize;
    private float roomWidthSize = 50.0f;
    private float roomHeightSize = 50.0f;

    //This is the grid to create the map itself.
    public GameObject[] mapGrid;

    //This is to determine the layout of the map.
    public bool mapOfDay;
    public bool randomMapChoice;
    public int mapSeed;

    //This is to determine the size of the map
    private Room[,] gridSize;

    void Start()
    {
        GenerateMap();
    }

    void Update()
    {

    }

    public GameObject RandomRoomLayout()
    {
        return mapGrid[UnityEngine.Random.Range(0, mapGrid.Length)];
    }

    public void GenerateMap()
    {
        if (mapOfDay)
        {
            DateTime dateTime = DateTime.Now;
            UnityEngine.Random.InitState(DateToInteger(dateTime));
        }
        else if (!randomMapChoice)
        {
            UnityEngine.Random.InitState(mapSeed);
        }
        //Determine the size of the map
        gridSize = new Room[colsSize, rowsSize];

        //Place the center point
        Vector3 centerOffset = new Vector3(roomWidthSize * (colsSize - 1) * 0.5f, 0.0f, roomHeightSize * (rowsSize - 1) * 0.5f);

        for (int i = 0; i < rowsSize; i++)
        {
            for (int j = 0; j < colsSize; j++)
            {
                //Placing the location.
                float mapXPosition = roomWidthSize * j;
                float mapZPosition = roomHeightSize * i;
                Vector3 newPosition = new Vector3(mapXPosition, 0.0f, mapZPosition);

                newPosition -= centerOffset;
                CreateTile(newPosition, i, j);
            }
        }

        //AdjustCameraPosition();
    }

    public int DateToInteger(DateTime dateSelected)
    {
        return dateSelected.Year + dateSelected.Month + dateSelected.Day;
    }

    void CreateTile(Vector3 position, int i, int j)
    {
        GameObject tempRoom = Instantiate(RandomRoomLayout(), position, Quaternion.identity) as GameObject;

        tempRoom.transform.parent = this.transform;

        tempRoom.name = "Section_" + j + "," + i;

        Room tempSection = tempRoom.GetComponent<Room>();

        OpenWall(tempSection, i, j);

        gridSize[j, i] = tempSection;
    }

    //The layout for 2x2 or greater.
    void OpenWall(Room room, int i, int j)
    {
        //This controls both upper and lower walls.
        if (i == 0)
        {
            //To deactivate the upperwall.
            room.doorNorth.SetActive(false);
        }
        else if (i == rowsSize - 1)
        {
            //To deactivate the lowerwall.
            room.doorSouth.SetActive(false);
        }
        else
        {
            //To deactivate both upper and lower walls.
            room.doorNorth.SetActive(false);
            room.doorSouth.SetActive(false);
        }

        //This controls both left and right walls.
        if (j == 0)
        {
            //To deactivate the eastern wall.
            room.doorEast.SetActive(false);
        }
        else if (j == colsSize - 1)
        {
            //To deactivate the western wall.
            room.doorWest.SetActive(false);
        }
        else
        {
            //To deactivate both eastern and western walls.
            room.doorEast.SetActive(false);
            room.doorWest.SetActive(false);
        }
    }


}